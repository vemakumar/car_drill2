const inventory = require("../data_2/inventory");

const countCarsBeforeYear = require("../problems_2/problem5");


// Test for Problem 5
const year = 2000; // Define the year
const olderCarsCount = countCarsBeforeYear(inventory, year);
console.log(`Number of cars made before ${year}: ${olderCarsCount}`);
