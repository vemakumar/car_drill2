// Problem 5
const countCarsBeforeYear = (inventory, year) => {
    const olderCarsCount = inventory.filter(car => car.car_year < year).length;
    return olderCarsCount;
};

module.exports = countCarsBeforeYear;
