// Problem 6
const filterBMWAndAudi = (inventory) => {
    const BMWAndAudi = inventory.filter(car => car.car_make === 'BMW' || car.car_make === 'Audi');
    return BMWAndAudi;
};

module.exports = filterBMWAndAudi;

