// Problem 2
const getLastCar = (inventory) => {
    const lastCar = inventory.reduce((acc, car) => car.id > acc.id ? car : acc);
    return `Last car is a ${lastCar.car_make} ${lastCar.car_model}`;
};

module.exports = getLastCar;
