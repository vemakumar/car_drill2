// Problem 1
const findCarById = (inventory, id) => {
    const car = inventory.filter(car => car.id === id)[0];
    return car ? `Car ${id} is a ${car.car_year} ${car.car_make} ${car.car_model}` : "Car not found";
};

module.exports = findCarById;
